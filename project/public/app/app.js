import { notaService as service } from './services/nota.service.js';
import { pipe, parameterHelper, handle, retry } from './utils/promise-helper.js';
import { takeUntil, debounce } from './utils/operator.js';
import { eventEmitter } from './utils/event-emitter.js';
import './utils/flatMap.js';

const action = pipe(
        parameterHelper(takeUntil, 3),
        parameterHelper(debounce, 500)
)

const operator = action(() => retry(3, 2000, () => handle(200, service.sumItem("2143")))
                                .then(data => eventEmitter.emit('NotasFiscais', data))
                                .catch(console. log));

//App
document.querySelector('#myButton')
        .onclick = operator;