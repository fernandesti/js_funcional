import { handlePromise, parameterHelper, pipe as _pipe } from '../utils/promise-helper.js';
import { Monad } from '../utils/Monad.js';

const URL = '//localhost:3000/notas';

            
const mapData = dataM => dataM.map(data => data.$flatMap(data => data.itens));
const filterByCode = (code, dataM) => dataM.map(data => data.filter(data => data.codigo === code));
const sumItems = dataM => dataM.map(data => data.reduce((total, item) => total + item.valor, 0));


export const notaService = {

    notaApi() {
            return fetch(URL)
                    .then(handlePromise)
                    .then(data => Monad.of(data))
                    .catch(err => {
                            console.log(err);
                            Promise.reject("Não foi possível obter notas.")
                    })
    },
    sumItem(code){

            //const sumNotas = filterByCode.bind(null, code);
            const filterByCodeFixed = parameterHelper(filterByCode, code);
            const pipe = _pipe(mapData, filterByCodeFixed, sumItems);
            
            return this.notaApi()
                    .then(pipe)
                    .then(data => data.getOrElse(0))
    }
}