export const log = data => {
    console.log(data);
    return data;
}

export const takeUntil = (time, fn) => 
                                        () => time-- > 0 && fn();

export const debounce = (time, fn) => {
    let control = 0
    return () => {
        clearTimeout(control);
        control = setTimeout(fn, time)
    }
}