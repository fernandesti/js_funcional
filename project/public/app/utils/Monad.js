export class Monad {

    constructor(value){
        this._value = value;
    }

    static of(value)
    {
        return new Monad(value);
    }

    isNullOrUndefined()
    {
        return this._value === null || this._value === undefined;
    }

    map(fn)
    {
        if(this.isNullOrUndefined()) return Monad.of(null);
        return Monad.of(fn(this._value));
    }

    getOrElse(value)
    {
        if(this.isNullOrUndefined()) return value;
        return this._value;
    }
}