export const handlePromise  = res => res.ok ? res.json() : Promise.reject(res.statusText);

export const parameterHelper = (fn, ...args) => fn.bind(null, ...args);

export const compose = (...fns) => data => fns.reduceRight((fn, next) => next(fn), data);

export const pipe = (...fns) => data => fns.reduce((fn, next) => next(fn), data);

export const handle = (milliseconds, fn) => {
    const timeout = new Promise((resolve, reject) => 
        setTimeout(() => reject(`Tempo de execução excedida - ${milliseconds}`), milliseconds));
        
        return Promise.race([
            timeout,
            fn
        ]);
};

const delay = milliseconds => data => 
    new Promise((resolve, reject) => {
        setTimeout(()=> resolve(data), milliseconds)
    });


export const retry = (retries, milliseconds, fn) =>
        fn().catch(err => {
        console.log(retries);
        return delay(milliseconds)().then(()=> retries > 1
            ? retry(--retries, milliseconds, fn)
            : Promise.reject(err)
    )})